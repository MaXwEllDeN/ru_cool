#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9           
#define SS_PIN          10          
#define BUZZER_PIN      8
#define TONE_FREQ       1800

MFRC522 mfrc522(SS_PIN, RST_PIN);   
MFRC522::MIFARE_Key key;

void setup() {
    Serial.begin(57600); 
    while (!Serial);    
    SPI.begin();        
    mfrc522.PCD_Init(); 
    Serial.println();   
}


void loop() {
    if (!mfrc522.PICC_IsNewCardPresent())
        return;

    if (!mfrc522.PICC_ReadCardSerial())
        return;
        
    tone(BUZZER_PIN, TONE_FREQ);
    delay(250);
    noTone(BUZZER_PIN);
    
	Serial.print(F(":"));
	String card_uid = "";  
  
	for (byte i = 0; i < mfrc522.uid.size; i++){
		card_uid.concat(String(mfrc522.uid.uidByte[i], HEX));
	}

	card_uid.concat("!");

	Serial.println(card_uid);
	mfrc522.PICC_HaltA();
}


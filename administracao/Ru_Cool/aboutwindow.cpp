#include "aboutwindow.h"
#include "ui_aboutwindow.h"
#include <QDesktopServices>
#include <QUrl>

AboutWindow::AboutWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutWindow)
{
    ui->setupUi(this);
}

AboutWindow::~AboutWindow()
{
    delete ui;
}

void AboutWindow::on_pushButton_fechar_clicked()
{
    this->hide();
    this->close();
}

void AboutWindow::on_pushButton_bibucket_clicked()
{
    QString link = "https://bitbucket.org/MaXwEllDeN/ru_cool";
    QDesktopServices::openUrl(link);
}

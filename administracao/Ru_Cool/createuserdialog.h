#ifndef CREATEUSERDIALOG_H
#define CREATEUSERDIALOG_H

#include <QDialog>
#include <QString>
#include <QFile>
#include <QNetworkReply>
#include <QNetworkAccessManager>

namespace Ui {
class CreateUserDialog;
}

class CreateUserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateUserDialog(QWidget *parent = 0);
    CreateUserDialog(QWidget *parent, QString matricula);
    ~CreateUserDialog();

private slots:
    void on_lineEdit_matricula_textChanged(const QString &arg1);
    void on_lineEdit_nome_textChanged(const QString &arg1);
    void on_lineEdit_curso_textChanged(const QString &arg1);

    void on_pushButton_carregarImagem_clicked();
    void on_pushButton_lerCartao_clicked();
    void on_pushButton_cancelar_clicked();
    void on_pushButton_salvar_clicked();

    void on_pictureDownloaded(QNetworkReply*);
    void on_uploadProgress(qint64, qint64);
    void on_uploadDone();

private:
    bool editInstance;
    Ui::CreateUserDialog *ui;
    QString localImagem;
    void uploadPicture(QString targetName);

    QFile* uploadFile;
    QString imgFormato;

    QNetworkAccessManager* networkAccess;
    QNetworkReply* networkReply;
    std::vector<QString> dados;
};

#endif // CREATEUSERDIALOG_H

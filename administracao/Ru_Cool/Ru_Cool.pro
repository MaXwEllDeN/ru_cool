#-------------------------------------------------
#
# Project created by QtCreator 2017-03-15T22:11:33
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Ru_Cool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    deskwindow.cpp \
    usuario.cpp \
    createuserdialog.cpp \
    tserial.cpp \
    readcardwindow.cpp \
    noticia.cpp \
    createnewsdialog.cpp \
    funcionario.cpp \
    createfuncionariodialog.cpp \
    aboutwindow.cpp

HEADERS  += mainwindow.h \
    database.h \
    deskwindow.h \
    usuario.h \
    createuserdialog.h \
    tserial.h \
    readcardwindow.h \
    noticia.h \
    createnewsdialog.h \
    funcionario.h \
    createfuncionariodialog.h \
    aboutwindow.h

FORMS    += mainwindow.ui \
    deskwindow.ui \
    createuserdialog.ui \
    readcardwindow.ui \
    createnewsdialog.ui \
    createfuncionariodialog.ui \
    aboutwindow.ui

QMAKE_CXXFLAGS +=  -std=c++11 -pthread

RESOURCES += \
    imagens.qrc

RC_FILE = myapp.rc

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "deskwindow.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    void start();
    Ui::MainWindow *ui;
    DeskWindow* deskWindow;
};

#endif // MAINWINDOW_H

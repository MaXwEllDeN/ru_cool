#ifndef CREATEFUNCIONARIODIALOG_H
#define CREATEFUNCIONARIODIALOG_H

#include <QDialog>
#include <QString>
#include <vector>

namespace Ui {
class CreateFuncionarioDialog;
}

class CreateFuncionarioDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateFuncionarioDialog(QWidget *parent = 0);
    CreateFuncionarioDialog(QWidget *parent, QString codigo);
    ~CreateFuncionarioDialog();

private slots:
    void on_pushButton_salvarFunc_clicked();

    void on_pushButton_cancelarFunc_clicked();

private:
    bool editInstance;
    Ui::CreateFuncionarioDialog *ui;
    std::vector<QString> dados;
};

#endif // CREATEFUNCIONARIODIALOG_H

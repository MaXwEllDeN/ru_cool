#ifndef FUNCIONARIO_H
#define FUNCIONARIO_H

#include <vector>
#include <QString>

class Funcionario
{
    std::vector<QString> dados;
public:
    Funcionario();

    Funcionario(QString matricula);
    void deletar();

    QString getNome() { return dados[1];}
};

#endif // FUNCIONARIO_H

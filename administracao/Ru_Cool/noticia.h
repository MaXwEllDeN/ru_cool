#ifndef NOTICIA_H
#define NOTICIA_H

#include <QFrame>
#include <QLabel>
#include <QTextBrowser>
#include <QVBoxLayout>
#include <QDate>

class Noticia : public QFrame
{
    Q_OBJECT
    QWidget* parent;
public:
    explicit Noticia(int noticia_id, QWidget *parent = 0);

    void setTitulo(QString titulo) { label_titulo->setText(titulo); }
    void setConteudo(QString texto) { text_conteudo->setText(texto); }
    void setInfo(QString info) { label_info->setText(info); }

    void loadFromDatabase();
    void deletar();

    QString getTitulo() { return label_titulo->text(); }
    QString getConteudo() { return text_conteudo->toPlainText(); }
    int getId() { return id; }

private slots:
    void on_editar();
    void on_deletar();

protected:
    int id;
    QLabel* label_titulo;
    QLabel* label_info;
    QTextBrowser* text_conteudo;

    QVBoxLayout* layout;
};

#endif // NOTICIA_H

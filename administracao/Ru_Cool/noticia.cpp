#include "noticia.h"
#include "database.h"

#include <QHBoxLayout>
#include <QSpacerItem>
#include <QFontMetrics>
#include <QPushButton>
#include <QStringList>
#include <QSignalMapper>
#include <QDebug>
#include <vector>

Noticia::Noticia(int noticia_id, QWidget *parent): QFrame(parent)
{
    this->parent = parent;
    this->id = noticia_id;

    label_titulo = new QLabel("SEM TíTULO");
    label_info = new QLabel("SEM INFORMAÇÕES");
    text_conteudo = new QTextBrowser;

    text_conteudo->setText("SEM CONTEÚDO");

    QPixmap imgEditar(":/imagens/icon_edit.png");
    QPixmap imgDeletar(":/imagens/icon_delete.png");

    // Popula a tabela com os botões

    QPushButton* editar = new QPushButton("Editar");
    QPushButton* deletar = new QPushButton("Deletar");

    editar->setIcon(QIcon(imgEditar));
    deletar->setIcon(QIcon(imgDeletar));

    editar->setToolTip("Editar notícia");
    deletar->setToolTip("Deletar notícia");

    connect(editar, SIGNAL(clicked()), this, SLOT(on_editar()));
    connect(deletar, SIGNAL(clicked()), this, SLOT(on_deletar()));

    layout = new QVBoxLayout;
    QHBoxLayout* hbox;

    hbox = new QHBoxLayout;

    hbox->addWidget(label_titulo);
    hbox->addStretch(1);
    hbox->addWidget(editar);
    hbox->addWidget(deletar);

    layout->addLayout(hbox);

    hbox = new QHBoxLayout;
    hbox->addWidget(text_conteudo);
    layout->addLayout(hbox);

    hbox = new QHBoxLayout;
    hbox->addWidget(label_info);
    hbox->setAlignment(Qt::AlignRight);
    layout->addLayout(hbox);

    this->setLayout(layout);
    //this->setStyleSheet("border-radius: 15px; border: 1px solid black; padding: 5px; background: #7286a5;");
    this->setStyleSheet("border-radius: 15px; border: 1px solid black; padding: 5px; background: #7286a5;");


    label_titulo->setStyleSheet("border: none; font-weight: bold; font-size: 12pt; background: none");

    text_conteudo->setStyleSheet("background: white; border-radius: 5px; font-size: 10pt;");
    text_conteudo->setContextMenuPolicy(Qt::CustomContextMenu);

    label_info->setStyleSheet("border: none; background: none; font-style: oblique;");

    editar->setStyleSheet("border: none; background: none");
    deletar->setStyleSheet("border: none; background: none");
    editar->adjustSize();

    hbox = NULL;

    this->setMinimumHeight(200);
    this->setMaximumHeight(200);
}

void Noticia::loadFromDatabase()
{
    std::vector<QString> dados = Database::getInstance()->returnData("SELECT titulo, texto, data FROM noticias WHERE id = :id;", 3, std::vector<QString>{QString::number(id)});
    setTitulo(dados[0]);
    setConteudo(dados[1]);

    QStringList dia = (dados[2].split("T")[0]).split("-");
    QStringList horario = (dados[2].split("T")[1]).split(":");

    setInfo("Postado em " + dia[2] + "/" + dia[1] + "/" + dia[0] + " às " + horario[0] + " hora(s) e " + horario[1] + " minuto(s).");
}

void Noticia::deletar()
{
    Database::getInstance()->execute("DELETE FROM noticias WHERE id = :id;", std::vector<QString>{QString::number(id)});
}

void Noticia::on_editar()
{
    QMetaObject::invokeMethod(parent, "on_editarNoticia", Qt::QueuedConnection, Q_ARG(int,  id));
}

void Noticia::on_deletar()
{
    QMetaObject::invokeMethod(parent, "on_deletarNoticia", Qt::QueuedConnection, Q_ARG(int,  id));
}


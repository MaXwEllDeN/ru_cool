#ifndef DESKWINDOW_H
#define DESKWINDOW_H

#include "database.h"

#include <QMainWindow>
#include <QObject>

namespace Ui {
class DeskWindow;
}

class DeskWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DeskWindow(QWidget *parent = 0);
    void setGerenteDisplayName(QString nome);
    ~DeskWindow();

private slots:
    void on_actionRelatorios_triggered();
    void on_actionUsuarios_triggered();
    void on_actionNoticias_triggered();
    void on_actionFuncionarios_triggered();

    void on_editarUsuario(const QString& matricula);
    void on_deletarUsuario(const QString& matricula);    
    void on_lineEdit_textChanged(const QString &arg1);
    void on_pushButton_cadastrar_clicked();

    void on_editarFuncionario(const QString& matricula);
    void on_deletarFuncionario(const QString& matricula);


    void on_editarNoticia(int id);
    void on_deletarNoticia(int id);
    void on_pushButton_adicionar_clicked();



    void on_lineEdit_funcionarios_textChanged(const QString &arg1);

    void on_pushButton_cadastrarFunc_clicked();

    void on_actionSobre_triggered();

    void on_dateEdit_dateChanged(const QDate &date);

private:
    Ui::DeskWindow *ui;
    void updateUsersView(QString text_query);
    void updateFuncsView(QString text_query);
    void updateNewsView();
    void updateRelatorio();

};

#endif // DESKWINDOW_H

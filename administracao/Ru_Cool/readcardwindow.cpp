#include "readcardwindow.h"
#include "ui_readcardwindow.h"
#include "tserial.h"

#include <thread>
#include <chrono>
#include <QList>
#include <QDebug>
#include <QFile>

void readSerial(ReadCardWindow* parent)
{
    Tserial *com;
    char buffer[10];

    try {
        while(1) {
            com = new Tserial();

            if (com != 0){
                com->connect(parent->getComport().toStdString().c_str(), 57600, spNONE);
                com->getArray(buffer, 10);
                com->disconnect();

                if ((buffer[0] == ':') && (buffer[9] == '!'))
                    break;
            }
        }

        delete com;
        com = 0;

        if (parent != NULL)
        {
            qDebug() << "Not null";
            QMetaObject::invokeMethod(parent, "onDataRead", Qt::QueuedConnection, Q_ARG(QString, QString(buffer).mid(1, 8)));
        }
        else
            qDebug() << "ADHEUDHAUDEA";
    }
    catch (std::bad_alloc& ba) {
        qDebug() << "bad_alloc caught: " << ba.what();
    }

}

ReadCardWindow::ReadCardWindow(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::ReadCardWindow)
{
    ui->setupUi(this);

    id = "";

    QFile file("config.dat");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);

    while (!in.atEnd()) {
        this->comport = in.readLine();
    }

    ui->label->setScaledContents(true);
    ui->label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    std::thread threadCartao(readSerial, this);
    threadCartao.detach();
}


void ReadCardWindow::onDataRead(QString id)
{
    this->id = id;
    ui->label_2->setText("Cartão identificado");
    ui->label_2->setStyleSheet("font-size:14pt; color:#248a12; text-align: center");
    ui->label->setEnabled(true);
    ui->label_2->move(ui->label_2->x() + 20, ui->label_2->y());

    ui->pushButton->setText("OK");
}


ReadCardWindow::~ReadCardWindow()
{
    delete ui;
}


void ReadCardWindow::on_pushButton_clicked()
{
    this->hide();
    this->close();
}

#include "createnewsdialog.h"
#include "ui_createnewsdialog.h"
#include "database.h"
#include "noticia.h"

#include <QMessageBox>
#include <QDebug>

CreateNewsDialog::CreateNewsDialog(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::CreateNewsDialog)
{
    ui->setupUi(this);
    noticia_id = 0;

}

CreateNewsDialog::CreateNewsDialog(int id, QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::CreateNewsDialog)
{
    Noticia news(id);
    news.loadFromDatabase();

    ui->setupUi(this);

    ui->lineEdit_titulo->setText(news.getTitulo());
    ui->plainTextEdit_conteudo->setPlainText(news.getConteudo());

    editInstance = true;
    noticia_id = news.getId();
}

CreateNewsDialog::~CreateNewsDialog()
{
    delete ui;
}

void CreateNewsDialog::on_pushButton_cancelar_clicked()
{
    this->hide();
    this->close();
}

void CreateNewsDialog::on_pushButton_publicar_clicked()
{

    QString titulo = ui->lineEdit_titulo->text();
    QString conteudo = ui->plainTextEdit_conteudo->toPlainText();

    if (titulo == "" || conteudo == "") {
        QMessageBox::information(this, "Dados insuficientes", "Todo o formulário deve ser preenchido!");
        return;
    }

    QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

    if (noticia_id) {
        std::vector<QString> param{titulo, conteudo, data, QString::number(noticia_id)};
        Database::getInstance()->execute("UPDATE noticias SET titulo = :titulo, texto = :texto, data = :data WHERE id = :id;", param);
        QMessageBox::about(this, "Notícia adicionada", "Notícia atualizada com sucesso!");
    }
    else {
        std::vector<QString> param{titulo, conteudo, data};
        Database::getInstance()->execute("INSERT INTO noticias(titulo, texto, data) VALUES (:titulo, :conteudo, :data);", param);
        QMessageBox::about(this, "Notícia adicionada", "Notícia adicionada com sucesso!");
    }




    this->hide();
    this->close();
}

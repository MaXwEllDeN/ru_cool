#ifndef READCARDWINDOW_H
#define READCARDWINDOW_H

#include <QDialog>
#include <QString>

namespace Ui {
class ReadCardWindow;
}

class ReadCardWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ReadCardWindow(QWidget *parent = 0);
    ~ReadCardWindow();
    QString getComport() { return comport; }
    QString getId() { return id; }

private slots:
    void onDataRead(QString id);

    void on_pushButton_clicked();

private:
    Ui::ReadCardWindow *ui;
    QString id;
    QString comport;
};

#endif // READCARDWINDOW_H

#include "createfuncionariodialog.h"
#include "ui_createfuncionariodialog.h"
#include "database.h"

#include <QMessageBox>
#include <QDebug>

CreateFuncionarioDialog::CreateFuncionarioDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateFuncionarioDialog)
{    
    editInstance = false;
    ui->setupUi(this);
}

CreateFuncionarioDialog::CreateFuncionarioDialog(QWidget *parent, QString codigo) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::CreateFuncionarioDialog)
{
    editInstance = true;

    ui->setupUi(this);
    std::vector<QString> dados = Database::getInstance()->returnData(tr("SELECT codigo, nome, senha FROM funcionarios")
                                                                     + " WHERE codigo = :cod;", 3, std::vector<QString>{codigo});
    this->setWindowTitle("Editar funcionário");

    ui->lineEdit_codigo->setText(dados[0]);
    ui->lineEdit_codigo->setEnabled(false);

    ui->lineEdit_nomeFunc->setText(dados[1]);
    ui->lineEdit_senhaFunc->setText(dados[2]);

}

CreateFuncionarioDialog::~CreateFuncionarioDialog()
{
    delete ui;
}

void CreateFuncionarioDialog::on_pushButton_salvarFunc_clicked()
{
    dados.clear();
    dados.push_back(ui->lineEdit_codigo->text());
    dados.push_back(ui->lineEdit_nomeFunc->text());
    dados.push_back(ui->lineEdit_senhaFunc->text());

    bool flag = true;


    for (QString val: dados) {
        if (val == "") {
            flag = false;
            break;
        }
    }

    if (!flag) {
        QMessageBox::information(this, "Dados insuficientes", "Todo o formulário deve ser preenchido!");
        return;
    }

    if (!editInstance) {
        std::vector<QString> verificar;
        verificar = Database::getInstance()->returnData("SELECT nome FROM funcionarios WHERE codigo = :cod;", 1, std::vector<QString>{dados[0]});

        if (verificar.size() > 0) {
            QMessageBox::information(this, "Código já cadastrado", "O código informado pertence a " + verificar[0] + ".");
            return;
        }

        Database::getInstance()->execute(QString("INSERT INTO funcionarios(codigo, nome, senha) ")
            + QString(" VALUES (:codigo, :nome, :senha);"), dados);

        QMessageBox::information(this, "Cadastro feito com sucesso", QString(dados[1]) + " cadastrado(a) com sucesso!");
        this->hide();
        this->close();
        return;
    }
    else {
        dados.push_back(dados[0]);
        Database::getInstance()->execute(tr("UPDATE funcionarios SET codigo = :cod, nome = :nome, senha = :senha")
            + " WHERE codigo = :cod;", dados);

        QMessageBox::information(this, "Edição feita com sucesso", QString(dados[1]) + " salvo(a) com sucesso!");
        this->hide();
        this->close();
        return;
    }

}
void CreateFuncionarioDialog::on_pushButton_cancelarFunc_clicked()
{
    this->hide();
    this->close();
}

#include "funcionario.h"
#include "database.h"

Funcionario::Funcionario()
{
    for (int i = 0; i < 3; i++)
        dados.push_back("");
}

Funcionario::Funcionario(QString codigo)
{
    this->dados = Database::getInstance()->returnData("SELECT codigo, nome, senha FROM funcionarios WHERE codigo = :cod;", 3, std::vector<QString>{codigo});
}

void Funcionario::deletar()
{
    Database::getInstance()->execute("DELETE FROM funcionarios WHERE codigo = :cod;", std::vector<QString>{dados[0]});
}

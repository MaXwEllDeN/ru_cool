#include "usuario.h"
#include "database.h"

Usuario::Usuario()
{
	for (int i = 0; i < 6; i++)
		dados.push_back("");
}

Usuario::Usuario(QString matricula)
{
    this->dados = Database::getInstance()->returnData("SELECT matricula, nome, email, curso, senha, cartao_id FROM usuarios WHERE matricula = :matricula;", 5, std::vector<QString>{matricula});
}

void Usuario::deletar()
{
    Database::getInstance()->execute("DELETE FROM usuarios WHERE matricula = :matricula;", std::vector<QString>{dados[0]});
}

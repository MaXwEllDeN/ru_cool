#ifndef USUARIO_H
#define USUARIO_H

#include <QString>
#include <vector>

class Usuario
{
private:
    std::vector<QString> dados;
public:
    Usuario();
    Usuario(QString matricula);
    void deletar();

    QString getNome() { return dados[1];}
};

#endif // USUARIO_H

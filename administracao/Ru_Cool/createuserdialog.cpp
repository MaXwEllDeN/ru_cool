#include "createuserdialog.h"
#include "ui_createuserdialog.h"
#include "readcardwindow.h"
#include "database.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QImage>
#include <QPixmap>
#include <QNetworkRequest>
#include <QUrl>


std::vector<QString> formatos{"jpg", "png", "jpeg", "bmp"};

CreateUserDialog::CreateUserDialog(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::CreateUserDialog)
{
    editInstance = false;

    ui->setupUi(this);
    localImagem = "";

    ui->progressBar->hide();
    ui->label_progresso->hide();
}

CreateUserDialog::CreateUserDialog(QWidget *parent, QString matricula) :
    QDialog(parent, Qt::WindowTitleHint),
    ui(new Ui::CreateUserDialog)
{
    editInstance = true;

    ui->setupUi(this);
    localImagem = "";

    std::vector<QString> dados = Database::getInstance()->returnData(tr("SELECT matricula, senha, nome, email, curso, cartao_id FROM usuarios")
                                                                     + " WHERE matricula = :matricula;", 6, std::vector<QString>{matricula});
    this->setWindowTitle("Editar usuário");


    ui->progressBar->hide();
    ui->label_progresso->hide();


    ui->lineEdit_matricula->setText(dados[0]);
    ui->lineEdit_matricula->setEnabled(false);

    ui->lineEdit_senha->setText(dados[1]);
    ui->lineEdit_nome->setText(dados[2]);
    ui->lineEdit_email->setText(dados[3]);
    ui->lineEdit_curso->setText(dados[4]);
    ui->label_cartaoId->setText(dados[5]);
    ui->label_cartaoId->setStyleSheet("color: darkgreen;");

    networkAccess = new QNetworkAccessManager(this);
    connect(networkAccess, SIGNAL(finished(QNetworkReply*)), this, SLOT(on_pictureDownloaded(QNetworkReply*)));

    //QUrl url("ftp://ftp.ihostfull.com/htdocs/imagens/" + dados[0]);
    QUrl url("ftp://192.168.173.1/htdocs/imagens/" + dados[0]);

    url.setUserName("maxwellden");
    url.setPassword("asdjkl123");
    //url.setUserName("uoolo_19845022");
    //url.setPassword("rucool");
    url.setPort(21);

    QNetworkRequest request(url);
    networkAccess->get(request);

}

void CreateUserDialog::on_pictureDownloaded(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error in " << reply->url() << ": " << reply->errorString();
        return;
    }

    QFile img("imagens/tmp");
    if (!img.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: " << img.error();
        return;
    }

    img.write(reply->readAll());
    img.close();

    ui->label_cartao_imagem->setStyleSheet("border-image: url(imagens/tmp) 0 0 0 0 stretch stretch; border: 2px; border-radius: 10px;");
    networkAccess->deleteLater();
}

CreateUserDialog::~CreateUserDialog()
{
    delete ui;
}

void CreateUserDialog::on_pushButton_carregarImagem_clicked()
{
    QString aux = QFileDialog::getOpenFileName(this,
                                               tr("Escolha a foto do usuário"), "C://Users//MaXwEllDeN//Pictures//", tr("Arquivos de imagem (*.png *.jpg *.bmp *jpeg)"));

    if (aux != "")
        localImagem = aux;

    QPixmap img(localImagem);

    if (!img.isNull()) {
        ui->label_cartao_imagem->setStyleSheet("border-image: url("+ localImagem +") 0 0 0 0 stretch stretch;" +
                                               "border: 2px; border-radius: 10px;");
    }
    else if(localImagem != "") {
        QMessageBox::warning(this, "Arquivo inválido", "A imagem escolhida não pôde ser carregada.");
    }

}

void CreateUserDialog::on_lineEdit_matricula_textChanged(const QString &arg1)
{
    ui->label_cartao_matricula->setText(arg1);
}

void CreateUserDialog::on_lineEdit_nome_textChanged(const QString &arg1)
{
    ui->label_cartao_nome->setText(arg1);
}

void CreateUserDialog::on_lineEdit_curso_textChanged(const QString &arg1)
{
    ui->label_cartao_curso->setText(arg1);
}

void CreateUserDialog::on_pushButton_cancelar_clicked()
{
    this->hide();
    this->close();
}

void CreateUserDialog::on_pushButton_lerCartao_clicked()
{
    ReadCardWindow lerCartao;
    lerCartao.exec();

    ui->label_cartaoId->setText(lerCartao.getId());
    ui->label_cartaoId->setStyleSheet("color: darkgreen;");

}

void CreateUserDialog::on_pushButton_salvar_clicked()
{
    dados.clear();
    dados.push_back(ui->lineEdit_matricula->text());
    dados.push_back(ui->lineEdit_senha->text());
    dados.push_back(ui->lineEdit_nome->text());
    dados.push_back(ui->lineEdit_email->text());
    dados.push_back(ui->lineEdit_curso->text());
    dados.push_back(ui->label_cartaoId->text());

    bool flag = true;

    if (localImagem == "" && !editInstance) {
        flag = false;
    }
    else {
        for (QString val: dados) {
            if (val == "") {
                flag = false;
                break;
            }
        }
    }

    if (!flag) {
        QMessageBox::information(this, "Dados insuficientes", "Todo o formulário deve ser preenchido!");
        return;
    }

    if (!editInstance) {
        std::vector<QString> verificar;
        verificar = Database::getInstance()->returnData("SELECT nome FROM usuarios WHERE matricula = :matricula;", 1, std::vector<QString>{dados[0]});

        if (verificar.size() > 0) {
            QMessageBox::information(this, "Matrícula já cadastrada", "A matrícula informada pertence a " + verificar[0] + ".");
            return;
        }

        Database::getInstance()->execute(QString("INSERT INTO usuarios(matricula, senha, nome, email, curso, cartao_id) ")
            + QString(" VALUES (:matricula, :senha, :nome, :email, :curso, :cartaoid);"), dados);
    }
    else {
        dados.push_back(dados[0]);
        Database::getInstance()->execute(tr("UPDATE usuarios SET matricula = :matricula, senha = :senha, nome = :nome, ")
            + " email = :email, curso = :curso, cartao_id = :cartaoid WHERE matricula = :mat;", dados);

        if (localImagem == "") {
            QMessageBox::information(this, "Edição feita com sucesso", QString(dados[2]) + " salvo(a) com sucesso!");
            this->hide();
            this->close();
            return;
        }
    }


    QImage img(localImagem);    
    QPixmap pixmap;
    imgFormato = QString(localImagem.split(".")[1]);

    pixmap = pixmap.fromImage(img.scaled(100, 130, Qt::IgnoreAspectRatio,Qt::SmoothTransformation));

    QFile file("imagens/tmp");
    file.open(QIODevice::WriteOnly);

    pixmap.save(&file, imgFormato.toStdString().c_str(), 100);
    file.close();

    uploadPicture(dados[0]);
}

void CreateUserDialog::uploadPicture(QString targetName)
{

    //QUrl url2("ftp://ftp.ihostfull.com/htdocs/imagens/" + targetName);

    //url2.setUserName("uoolo_19845022");
    //url2.setPassword("rucool");

    QUrl url2("ftp://192.168.173.1/htdocs/imagens/" + targetName);

    url2.setUserName("maxwellden");
    url2.setPassword("asdjkl123");
    url2.setPort(21);

    uploadFile = new QFile("imagens/tmp");
    uploadFile->open(QIODevice::ReadOnly);

    ui->progressBar->show();
    ui->label_progresso->show();

    networkAccess = new QNetworkAccessManager();
    networkReply = networkAccess->put(QNetworkRequest(url2), uploadFile);

    connect(networkReply, SIGNAL(uploadProgress(qint64, qint64)), SLOT(on_uploadProgress(qint64,qint64)));
    connect(networkReply, SIGNAL(finished()), SLOT(on_uploadDone()));
}

void CreateUserDialog::on_uploadProgress(qint64 done, qint64 total)
{
    int percent = 0;

    if (total > 0)
        percent = ceil((done * 100) / total);

    ui->progressBar->setValue(percent);

    if (percent == 100) {
        if (!editInstance)
            QMessageBox::information(this, "Cadastro feito com sucesso", QString(dados[2]) + " cadastrado(a) com sucesso!");
        else
            QMessageBox::information(this, "Edição feita com sucesso", QString(dados[2]) + " editado(a) com sucesso!");

        this->hide();
        this->close();
    }

}

void CreateUserDialog::on_uploadDone()
{
    networkAccess->deleteLater();
    networkReply->deleteLater();
    uploadFile->deleteLater();
}

#include "deskwindow.h"
#include "ui_deskwindow.h"
#include "usuario.h"
#include "funcionario.h"
#include "createuserdialog.h"
#include "createnewsdialog.h"
#include "createfuncionariodialog.h"
#include "noticia.h"
#include "aboutwindow.h"

#include <QSqlQueryModel>
#include <QMessageBox>
#include <QModelIndex>
#include <QPushButton>
#include <QSignalMapper>
#include <QSqlError>
#include <QVBoxLayout>
#include <QDebug>
#include <QDesktopWidget>

DeskWindow::DeskWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DeskWindow)
{
    ui->setupUi(this);

    // Configuração dos ítens gráficos
    ui->imglabel_logo->setScaledContents(true);
    ui->imglabel_logo->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    //Configurando o ambiente
    ui->toolBar->setIconSize(QSize(64,64));
    updateRelatorio();
}

DeskWindow::~DeskWindow()
{
    delete ui;
}

void DeskWindow::on_actionRelatorios_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
    updateRelatorio();
}


void DeskWindow::on_actionUsuarios_triggered()
{
    ui->stackedWidget->setCurrentIndex(1);
    updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios ORDER BY nome ASC LIMIT 10;");
}


void DeskWindow::updateNewsView()
{
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    QVBoxLayout* vbox = new QVBoxLayout;

    vbox->setSpacing(40);
    Noticia* noticia = NULL;

    QSqlQuery* query = new QSqlQuery(*(Database::getInstance()->getQtDatabase()));
    query->prepare("SELECT id FROM noticias ORDER BY data DESC LIMIT 10;");
    query->exec();


    while(query->next()) {
        noticia = new Noticia(query->value(0).toInt(), this);
        noticia->loadFromDatabase();
        vbox->addWidget(noticia);
    }

    if (noticia == NULL) {
        QLabel* label = new QLabel("Nenhuma notícia cadastrada.");
        label->setStyleSheet("font-size: 12pt;");
        vbox->setAlignment(Qt::AlignCenter);
        vbox->addWidget(label);
    }


    noticia = NULL;

    QLayout * layout = ui->noticiasContents->layout();



    if (layout != NULL) {
        QLayoutItem* item;
        QLayout* sublayout;
        QWidget* widget;
        while ((item = layout->takeAt(0))) {
            if ((sublayout = item->layout()) != 0) {/* do the same for sublayout*/}
            else if ((widget = item->widget()) != 0) {widget->hide(); delete widget;}
            else {delete item;}
        }

        delete layout;
    }

    ui->noticiasContents->setLayout(vbox);
}

void DeskWindow::on_actionNoticias_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);
    updateNewsView();
}


void DeskWindow::on_pushButton_adicionar_clicked()
{
    CreateNewsDialog window(this);
    window.exec();

    updateNewsView();
}

void DeskWindow::on_editarNoticia(int id)
{
    CreateNewsDialog window(id, this);
    window.exec();

    updateNewsView();
}

void DeskWindow::on_deletarNoticia(int id)
{
    Noticia aux(id);
    aux.loadFromDatabase();

    QMessageBox::StandardButton resposta;
    resposta = QMessageBox::question(this, "Atenção", "Pressione OK para prosseguir e deletar a notícia '" + aux.getTitulo() + "'."
                                     , QMessageBox::Ok | QMessageBox::Cancel);

    if (resposta == QMessageBox::Ok) {
        aux.deletar();
        updateNewsView();;
        QMessageBox::about(this, "Atenção", "Notícia deletada com sucesso!");
    }
}

void DeskWindow::updateUsersView(QString text_query)
{
    Database* db = Database::getInstance();

    QSqlQueryModel* modal = new QSqlQueryModel();

    QSqlQuery* query = new QSqlQuery(*(db->getQtDatabase()));
    query->prepare(text_query);

    query->exec();
    modal->setQuery(*query);


    modal->insertColumn(5);
    modal->setHeaderData(5, Qt::Horizontal, "");

    modal->insertColumn(6);
    modal->setHeaderData(6, Qt::Horizontal, "");


    ui->tableView_usuarios->setModel(modal);

    QPixmap imgEditar(":/imagens/icon_edit.png");
    QPixmap imgDeletar(":/imagens/icon_delete.png");

    // Popula a tabela com os botões

    QPushButton* editar = NULL;
    QPushButton* deletar = NULL;
    QSignalMapper* signalMapper = NULL;


    for (int i = 0; i < modal->rowCount(); i++) {
        signalMapper = new QSignalMapper(this);
        editar = new QPushButton;
        deletar = new QPushButton;

        editar->setFlat(true);
        deletar->setFlat(true);

        editar->setIcon(QIcon(imgEditar));
        deletar->setIcon(QIcon(imgDeletar));

        editar->setToolTip("Editar usuário");
        deletar->setToolTip("Deletar usuário");

        ui->tableView_usuarios->setIndexWidget(modal->index(i, 5), editar);
        ui->tableView_usuarios->setIndexWidget(modal->index(i, 6), deletar);

        // Registrando o botão de editar
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_editarUsuario(QString)));
        connect(editar, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(editar, modal->index(i, 0).data().toString());

        // Registrando o botão de deletar
        signalMapper = new QSignalMapper(this);
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_deletarUsuario(QString)));
        connect(deletar, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(deletar, modal->index(i, 0).data().toString());


        editar = NULL;
        deletar = NULL;

        signalMapper = NULL;
    }


    ui->tableView_usuarios->resizeColumnsToContents();
    ui->tableView_usuarios->setShowGrid(false);
    ui->tableView_usuarios->setColumnWidth(1, 180);
    ui->tableView_usuarios->setColumnWidth(2, 220);
    ui->tableView_usuarios->setColumnWidth(3, 150);
    ui->tableView_usuarios->setColumnWidth(4, 111);
    ui->tableView_usuarios->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

}


void DeskWindow::updateFuncsView(QString text_query)
{
    Database* db = Database::getInstance();

    QSqlQueryModel* modal = new QSqlQueryModel();

    QSqlQuery* query = new QSqlQuery(*(db->getQtDatabase()));
    query->prepare(text_query);

    query->exec();
    modal->setQuery(*query);


    modal->insertColumn(3);
    modal->setHeaderData(3, Qt::Horizontal, "");

    modal->insertColumn(4);
    modal->setHeaderData(4, Qt::Horizontal, "");


    ui->tableView_funcionarios->setModel(modal);

    QPixmap imgEditar(":/imagens/icon_edit.png");
    QPixmap imgDeletar(":/imagens/icon_delete.png");

    // Popula a tabela com os botões

    QPushButton* editar = NULL;
    QPushButton* deletar = NULL;
    QSignalMapper* signalMapper = NULL;


    for (int i = 0; i < modal->rowCount(); i++) {
        signalMapper = new QSignalMapper(this);
        editar = new QPushButton;
        deletar = new QPushButton;

        editar->setFlat(true);
        deletar->setFlat(true);

        editar->setIcon(QIcon(imgEditar));
        deletar->setIcon(QIcon(imgDeletar));

        editar->setToolTip("Editar usuário");
        deletar->setToolTip("Deletar usuário");

        ui->tableView_funcionarios->setIndexWidget(modal->index(i, 3), editar);
        ui->tableView_funcionarios->setIndexWidget(modal->index(i, 4), deletar);

        // Registrando o botão de editar
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_editarFuncionario(QString)));
        connect(editar, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(editar, modal->index(i, 0).data().toString());

        // Registrando o botão de deletar
        signalMapper = new QSignalMapper(this);
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_deletarFuncionario(QString)));
        connect(deletar, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(deletar, modal->index(i, 0).data().toString());


        editar = NULL;
        deletar = NULL;

        signalMapper = NULL;
    }


    ui->tableView_funcionarios->resizeColumnsToContents();
    ui->tableView_funcionarios->setShowGrid(false);
    ui->tableView_funcionarios->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
}

void DeskWindow::on_lineEdit_textChanged(const QString &arg1)
{
    if (arg1.toInt()) {
        updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios WHERE CAST(matricula as TEXT) LIKE '" + arg1 + "%' ORDER BY nome ASC LIMIT 10;");
    }
    else
        updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios WHERE nome LIKE '" + arg1 + "%' ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_pushButton_cadastrar_clicked()
{
    CreateUserDialog window(this);
    window.exec();

    updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_editarUsuario(const QString& matricula)
{
    CreateUserDialog window(this, matricula);
    window.exec();

    updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_deletarUsuario(const QString& matricula)
{
    Usuario usr(matricula);

    QMessageBox::StandardButton resposta;
    resposta = QMessageBox::question(this, "Atenção", "Pressione OK para prosseguir e deletar o usuário " + usr.getNome()
                                     , QMessageBox::Ok | QMessageBox::Cancel);


    if (resposta == QMessageBox::Ok) {
        usr.deletar();
        updateUsersView("SELECT matricula, nome, email, curso, senha FROM usuarios ORDER BY nome ASC LIMIT 10;");
        QMessageBox::about(this, "Atenção", usr.getNome() + " deletado com sucesso!");
    }

}

void DeskWindow::setGerenteDisplayName(QString nome)
{
    ui->label_funcionario->setText(nome);
    ui->label_funcionario->setStyleSheet("font-size:9pt; font-weight:600;");
}

void DeskWindow::on_actionFuncionarios_triggered()
{
    ui->stackedWidget->setCurrentIndex(3);
    updateFuncsView("SELECT codigo, nome, senha FROM funcionarios ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_editarFuncionario(const QString& codigo)
{
    CreateFuncionarioDialog window(this, codigo);
    window.exec();

    updateFuncsView("SELECT codigo, nome, senha FROM funcionarios ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_deletarFuncionario(const QString& codigo)
{

    QSqlDatabase* db = Database::getInstance()->getQtDatabase();
    QSqlQuery query = db->exec("SELECT codigo FROM funcionarios LIMIT 2;");

    int count = 0;

    while (query.next())
        count++;

    db = NULL;

    if (count == 1) {
        QMessageBox::warning(this, "Erro", "É necessário ter ao menos um funcionário cadastrado.");
        return;
    }

    Funcionario func(codigo);

    QMessageBox::StandardButton resposta;
    resposta = QMessageBox::question(this, "Atenção", "Pressione OK para prosseguir e deletar o funcionario " + func.getNome()
                                     , QMessageBox::Ok | QMessageBox::Cancel);

    if (resposta == QMessageBox::Ok) {
        func.deletar();
        updateFuncsView("SELECT codigo, nome, senha FROM funcionarios ORDER BY nome ASC LIMIT 10;");
        QMessageBox::about(this, "Atenção", func.getNome() + " deletado com sucesso!");
    }

}

void DeskWindow::on_lineEdit_funcionarios_textChanged(const QString &arg1)
{
    if (arg1.toInt()) {
        updateFuncsView("SELECT codigo, nome, senha FROM funcionarios WHERE CAST(codigo as TEXT) LIKE '" + arg1 + "%' ORDER BY nome ASC LIMIT 10;");
    }
    else
        updateFuncsView("SELECT codigo, nome, senha FROM funcionarios WHERE nome LIKE '" + arg1 + "%' ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::on_pushButton_cadastrarFunc_clicked()
{
    CreateFuncionarioDialog window(this);
    window.exec();

    updateFuncsView("SELECT codigo, nome, senha FROM funcionarios ORDER BY nome ASC LIMIT 10;");
}

void DeskWindow::updateRelatorio()
{

    Database* db = Database::getInstance();

    QDateTime dateT = ui->dateEdit->dateTime();
    QString data = dateT.toString("yyyy-MM-dd") + " 00:00:00";

    QStringList aux2 = data.split(" ")[0].split("-");
    QString data2 = aux2[0] + "-"+ aux2[1] + "-" + QString::number(aux2[2].toInt() + 1) + " 00:00:00";

            QSqlQuery query = db->getQtDatabase()->exec();
            query.prepare("SELECT avaliacao, tipo FROM refeicoes WHERE data > '" + data + "' AND data < '"+ data2 +"';");

            query.exec();

            int qnt_avaliacoes = 0;
            double avaliacao = 0;
            int jantar = 0;
            int almoco = 0;

            while (query.next()) {
                avaliacao += query.value(0).toFloat();
                qnt_avaliacoes++;

                if (query.value(1).toInt() == 1)
                    almoco++;
                else if (query.value(1).toInt() == 2)
                    jantar++;
            }

            ui->label_almoco->setText(QString::number(almoco));
            ui->label_jantar->setText(QString::number(jantar));

            if (qnt_avaliacoes != 0) {
                qDebug() << (avaliacao / qnt_avaliacoes);
                ui->label_avaliacao->setText(QString::number(avaliacao / qnt_avaliacoes, 'f', 2));
            } else
                ui->label_avaliacao->setText(QString::number(0, 'f', 2));


}

void DeskWindow::on_actionSobre_triggered()
{
    AboutWindow window(this);
    window.exec();
}

void DeskWindow::on_dateEdit_dateChanged(const QDate &date)
{
    updateRelatorio();
}

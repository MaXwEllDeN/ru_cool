#ifndef CREATENEWSDIALOG_H
#define CREATENEWSDIALOG_H

#include <QDialog>

namespace Ui {
class CreateNewsDialog;
}

class CreateNewsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateNewsDialog(QWidget *parent = 0);
    explicit CreateNewsDialog(int id, QWidget *parent = 0);
    ~CreateNewsDialog();

private slots:
    void on_pushButton_cancelar_clicked();

    void on_pushButton_publicar_clicked();

private:
    Ui::CreateNewsDialog *ui;
    bool editInstance;
    int noticia_id;
};

#endif // CREATENEWSDIALOG_H

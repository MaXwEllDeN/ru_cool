#include "database.h"

#include <QSqlResult>
#include <QSqlError>
#include <QDebug>
#include <QMessageBox>

Database *Database::instance = NULL;

Database* Database::getInstance()
{
    if (!instance) {
        instance = new Database;
        instance->connect();
    }

    return instance;
}

void Database::connect()
{
    mDatabase = QSqlDatabase::addDatabase("QPSQL");

    mDatabase.setDatabaseName("ru_cool");
    mDatabase.setHostName("192.168.173.1");
    mDatabase.setUserName("postgres");
    mDatabase.setPassword("asdjkl123");
    mDatabase.setPort(5432);

    mQuery = QSqlQuery();


    if (!mDatabase.open()) {
        return;
    }

}

QSqlDatabase* Database::getQtDatabase()
{
    return &mDatabase;
}

std::vector<QString> Database::returnData(QString query, int results, std::vector<QString> vec)
{
    std::vector<QString> aux;
    mQuery.prepare(query);


    int i = 0;
    for (QString elemento: vec) {
        mQuery.bindValue(i, elemento);
        i++;
    }

    mQuery.exec();

    if (mQuery.next()) {
        for (int i = 0; i < results; i++) {
            aux.push_back(mQuery.value(i).toString());
        }
    }

    return aux;
}

void Database::execute(QString query, std::vector<QString> vec)
{
    mQuery.prepare(query);

    int i = 0;
    for (QString elemento: vec) {
        mQuery.bindValue(i, elemento);
        i++;
    }

    mQuery.exec();
}

bool Database::queryCheck(QString query, std::vector<QString> vec)
{
    mQuery.prepare(query);

    int i = 0;
    for (QString elemento: vec) {
        mQuery.bindValue(i, elemento);
        i++;
    }

    mQuery.exec();

    if (mQuery.next())
        return true;

    return false;
}

void Database::close()
{
    mDatabase.close();
}

#ifndef USERVIEWDIALOG_H
#define USERVIEWDIALOG_H

#include "usuario.h"

#include <QDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
class UserViewDialog;
}

class UserViewDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserViewDialog(QWidget*, QString matricula);
    ~UserViewDialog();

private slots:
    void on_pushButton_nao_clicked();
    void on_pictureDownloaded(QNetworkReply*);

    void on_pushButton_sim_clicked();

private:
    Ui::UserViewDialog *ui;
    QNetworkAccessManager* networkAccess;
    QNetworkReply* networkReply;
    Usuario* usuario;
};

#endif // USERVIEWDIALOG_H

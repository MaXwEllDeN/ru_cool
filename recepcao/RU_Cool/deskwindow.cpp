#include "deskwindow.h"
#include "ui_deskwindow.h"
#include "database.h"
#include "tserial.h"
#include "refeicao.h"
#include "userviewdialog.h"
#include "aboutwindow.h"

#include <chrono>
#include <thread>
#include <QTableWidgetItem>
#include <QStandardItemModel>
#include <QSqlError>
#include <vector>
#include <QString>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QModelIndex>
#include <QPushButton>
#include <QSignalMapper>
#include <QDateTime>
#include <QNetworkRequest>
#include <QUrl>
#include <QFile>

#include <QDebug>
#include <QPainter>
#include <QPixmap>

QString readSerial();
void waitUserThread(DeskWindow* parent);
void updateAvaliacao(DeskWindow* parent);

DeskWindow::DeskWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DeskWindow)
{
    ui->setupUi(this);
    updateLastUsers();

    QFile file("config.dat");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);

    while (!in.atEnd()) {
        this->comport = in.readLine();
    }

    std::thread card_thread(waitUserThread, this);
    card_thread.detach();

    std::thread avaliacao_thread(updateAvaliacao, this);
    avaliacao_thread.detach();
}

void DeskWindow::updateLastUsers()
{
    std::vector<std::vector<QString>> comensais;
    Database* db = Database::getInstance();
    QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QStringList aux = data.split(" ")[1].split(":");

    unsigned int hours = aux[0].toInt() - 5;

    if (hours > 24)
        hours = 0;

    data = data.split(" ")[0] + " " + QString::number(hours) + ":" + aux[1] + ":" + aux[2];

    QSqlQuery query = db->getQtDatabase()->exec();
    query.prepare("SELECT matricula FROM refeicoes WHERE data > '" + data + "' ORDER BY data DESC LIMIT 20;");
    query.exec();

    while (query.next())
        comensais.push_back(db->returnData("SELECT matricula, nome, curso FROM usuarios WHERE matricula = :mat",
                                                3, std::vector<QString>{ query.value(0).toString() }));

    QTableWidget* table = ui->tableWidget_usuarios;

    table->setRowCount(comensais.size());
    table->setColumnCount(4);
    table->setHorizontalHeaderLabels(QString("Matrícula;Nome;Curso;").split(";"));

    QTableWidgetItem* item = NULL;

    int counter = 0;
    QPixmap imgDeletar(":/imagens/icon_delete.png");

    QPushButton* cancelar = NULL;
    QSignalMapper* signalMapper = NULL;

    for (auto it: comensais) {
        item = new QTableWidgetItem(it[0]);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(counter, 0, item);

        item = new QTableWidgetItem(it[1]);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(counter, 1, item);

        item = new QTableWidgetItem(it[2]);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(counter, 2, item);

        cancelar = new QPushButton("");
        cancelar->setIcon(QIcon(imgDeletar));
        cancelar->setToolTip("Cancelar entrada");
        cancelar->setFixedWidth(32);
        cancelar->setFlat(true);

        signalMapper = new QSignalMapper(this);
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_userClicked(QString)));
        connect(cancelar, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(cancelar, it[0]);

        table->setCellWidget(counter, 3, cancelar);
        cancelar = NULL;
        signalMapper = NULL;

        counter++;
    }

    item = NULL;


    table->setShowGrid(false);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    table->setColumnWidth(1, 300);
    table->setColumnWidth(2, 210);
    table->setColumnWidth(3, 32);
}

void DeskWindow::on_userClicked(QString matricula)
{
    UserViewDialog window(this, matricula);
    window.exec();
    updateLastUsers();
}

DeskWindow::~DeskWindow()
{
    delete ui;
}

QString readSerial(DeskWindow* parent)
{
    Tserial *com;
    char buffer[10];

    try {
        while(1) {
            com = new Tserial();

            if (com != 0){
                com->connect(parent->getComport().toStdString().c_str(), 57600, spNONE);
                com->getArray(buffer, 10);
                com->disconnect();

                if ((buffer[0] == ':') && (buffer[9] == '!'))
                    break;
            }
        }

        delete com;
        com = 0;

        return QString(buffer).mid(1, 8);
    }
    catch (std::bad_alloc& ba) {
        qDebug() << "bad_alloc caught: " << ba.what();
    }

    return "";
}

int DeskWindow::getAvaliacao()
{
    return ceil((ui->label_estrela->width() / 64.0) * 100);
}

void DeskWindow::setAvaliacao(int percent)
{
    float w = 64 * (float)(percent / 100.0);
    ui->label_estrela->setGeometry(ui->label_estrela->x(), ui->label_estrela->y(), (int) w, ui->label_estrela->height());
    ui->label_avaliacao->setText(QString::number(5 * percent / 100.0, 'f', 2));
    ui->label_avaliacao->setStyleSheet("font-size:18pt; font-weight:600; color:#35a716;");
}

void DeskWindow::onDataRead(QString matricula)
{

    if (matricula != "") {
        Usuario usr(matricula);
        Refeicao refeicao = usr.getRecentRefeicao();

        QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
        QStringList aux = data.split(" ")[1].split(":");

        refeicao.setData(data);
        refeicao.setMatricula(matricula);

        if (aux[0].toInt() < 15)
            refeicao.setTipo(1);
        else
            refeicao.setTipo(2);

        refeicao.setAvaliacao(-1);
        refeicao.adicionar();


        networkAccess = new QNetworkAccessManager(this);
        connect(networkAccess, SIGNAL(finished(QNetworkReply*)), this, SLOT(on_pictureDownloaded(QNetworkReply*)));

        /*
        QUrl url("ftp://ftp.ihostfull.com/htdocs/imagens/" + matricula);

        url.setUserName("uoolo_19845022");
        url.setPassword("rucool");
        */

        QUrl url("ftp://192.168.173.1/htdocs/imagens/" + matricula);

        url.setUserName("maxwellden");
        url.setPassword("asdjkl123");
        url.setPort(21);

        QNetworkRequest request(url);
        networkAccess->get(request);


        ui->label_cartao_nome->setText(usr.getNome());
        ui->label_cartao_matricula->setText(usr.getMatricula());
        ui->label_cartao_curso->setText(usr.getCurso());
        updateLastUsers();
    }
}

void DeskWindow::on_pictureDownloaded(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error in " << reply->url() << ": " << reply->errorString();
        return;
    }

    QFile img("imagens/tmp");
    if (!img.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: " << img.error();
        return;
    }

    img.write(reply->readAll());
    img.close();

    ui->label_cartao_imagem->setStyleSheet("border-image: url(imagens/tmp) 0 0 0 0 stretch stretch; border: 2px; border-radius: 10px;");
    networkAccess->deleteLater();
}

void waitUserThread(DeskWindow* parent)
{
    while (1) {
        QString cardId = readSerial(parent);

        if (cardId != "") {
            QSqlQuery query = Database::getInstance()->getQtDatabase()->exec();
            query.prepare("SELECT matricula FROM usuarios WHERE cartao_id = :id LIMIT 1;");
            query.bindValue(0, cardId);
            query.exec();

            if (query.next()) {

                QString matricula = query.value(0).toString();
                Usuario usr(matricula);

                query.clear();

                if (usr.getRecentRefeicao().getId() != 0) {
                    matricula = "";
                }

                QMetaObject::invokeMethod(parent, "onDataRead", Qt::QueuedConnection, Q_ARG(QString, matricula));
            }

            query.clear();
        }
    }
}


void updateAvaliacao(DeskWindow* parent)
{

    while (1) {
        Database* db = Database::getInstance();
        QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
        QStringList aux = data.split(" ")[1].split(":");

        unsigned int hours = aux[0].toInt() - 5;

        if (hours > 24)
            hours = 0;

        int tipo = 0;
        if (aux[0].toInt() < 15)
            tipo = 1;
        else
            tipo = 2;

        data = data.split(" ")[0] + " " + QString::number(hours) + ":" + aux[1] + ":" + aux[2];

        QSqlQuery query = db->getQtDatabase()->exec();
        query.prepare("SELECT avaliacao FROM refeicoes WHERE data > '" + data + "' AND tipo = '" + QString::number(tipo) + "' AND avaliacao > -1;");
        query.exec();

        int qnt_avaliacoes = 0;
        double avaliacao = 0;

        while (query.next()) {
            avaliacao += query.value(0).toFloat();
            qnt_avaliacoes++;
        }

        if (qnt_avaliacoes != 0) {
            QMetaObject::invokeMethod(parent, "setAvaliacao", Qt::QueuedConnection, Q_ARG(int, (avaliacao / qnt_avaliacoes) * 100 / 5));
        }
        else {
            QMetaObject::invokeMethod(parent, "setAvaliacao", Qt::QueuedConnection, Q_ARG(int, 100));
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void DeskWindow::on_actionSobre_triggered()
{
    AboutWindow window(this);
    window.exec();
}

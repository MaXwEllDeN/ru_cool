#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "database.h"

#include <vector>
#include <QMessageBox>
#include <vector>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    try {
        // Configuração dos ítens gráficos
        ui->imglabel_logo->setScaledContents(true);
        ui->imglabel_logo->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    }
    catch(...) {
        QMessageBox::critical(this, "Erro", "Não foi possível conectar-se ao banco de dados.");
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    QString codigo = ui->lineEdit_codigo->text();
    QString senha = ui->lineEdit_senha->text();
    std::vector<QString> dados = Database::getInstance()->returnData("SELECT * FROM funcionarios WHERE codigo = :codigo AND senha = :senha;", 3, std::vector<QString>{codigo, senha});

    if (dados.size() > 0) {
        hide();
        deskWindow = new DeskWindow;
        //deskWindow->setGerenteDisplayName(dados[1]);
        deskWindow->show();
    }
    else {
        QMessageBox::warning(this, "Dados incorretos", "O código e senha informados não coincidem.");
    }

}

#include "userviewdialog.h"
#include "ui_userviewdialog.h"

#include <QUrl>
#include <QFile>

UserViewDialog::UserViewDialog(QWidget *parent, QString matricula) :
    QDialog(parent),
    ui(new Ui::UserViewDialog)
{
    ui->setupUi(this);
    this->usuario = new Usuario(matricula);

    QUrl url("ftp://192.168.173.1/htdocs/imagens/" + matricula);

    networkAccess = new QNetworkAccessManager(this);
    connect(networkAccess, SIGNAL(finished(QNetworkReply*)), this, SLOT(on_pictureDownloaded(QNetworkReply*)));

    url.setUserName("maxwellden");
    url.setPassword("asdjkl123");
    url.setPort(21);

    QNetworkRequest request(url);
    networkAccess->get(request);

    ui->label_cartao_nome->setText(usuario->getNome());
    ui->label_cartao_matricula->setText(matricula);
    ui->label_cartao_curso->setText(usuario->getCurso());

    this->setWindowTitle("Analisando " + usuario->getNome());
}

void UserViewDialog::on_pictureDownloaded(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error in " << reply->url() << ": " << reply->errorString();
        return;
    }

    QFile img("imagens/tmp2");
    if (!img.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: " << img.error();
        return;
    }

    img.write(reply->readAll());
    img.close();

    ui->label_cartao_imagem->setStyleSheet("border-image: url(imagens/tmp2) 0 0 0 0 stretch stretch; border: 2px; border-radius: 10px;");
    networkAccess->deleteLater();
}

UserViewDialog::~UserViewDialog()
{
    delete ui;
}

void UserViewDialog::on_pushButton_nao_clicked()
{
    this->hide();
    this->close();
}

void UserViewDialog::on_pushButton_sim_clicked()
{
    usuario->getRecentRefeicao().deletar();
    this->hide();
    this->close();
}

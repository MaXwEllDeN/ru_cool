#include "refeicao.h"
#include "database.h"
#include <QDebug>

Refeicao::Refeicao(int id_param) {
    std::vector<QString> dados = Database::getInstance()->returnData("SELECT id, tipo, avaliacao, matricula, data FROM refeicoes WHERE id = :id;",
                                                                     5, std::vector<QString>{QString::number(id_param)});

    if (dados.size() == 0) {
        this->id = 0;
        return;
    }

    this->id = dados[0].toInt();
    this->tipo = dados[1].toInt();
    this->avaliacao = dados[2].toInt();
    this->matricula_usuario = dados[3];
    this->data = dados[4];
}

Refeicao::Refeicao(std::vector<QString> dados)
{
    this->id = dados[0].toInt();
    this->tipo = dados[1].toInt();
    this->avaliacao = dados[2].toInt();
    this->matricula_usuario = dados[3];
    this->data = dados[4];
}

void Refeicao::adicionar()
{
    QString sql = "INSERT INTO refeicoes(matricula, data, tipo, avaliacao) VALUES (" +
            matricula_usuario + ", '" + data + "', " + QString::number(tipo) + ", " + QString::number(avaliacao) + ");";

    Database::getInstance()->execute(sql, std::vector<QString>{});
}

void Refeicao::deletar()
{
    Database::getInstance()->execute("DELETE FROM refeicoes WHERE id = :place;", std::vector<QString>{QString::number(id)});
}

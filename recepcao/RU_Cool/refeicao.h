#ifndef REFEICAO_H
#define REFEICAO_H

#include <QString>
#include <vector>

class Refeicao
{
private:
    int id;
    int tipo;
    int avaliacao;
    QString matricula_usuario;
    QString data;

public:
    Refeicao(int id);
    Refeicao(std::vector<QString>);

    int getId() { return id; }

    void setData(QString ndata) { this->data = ndata; }
    void setMatricula(QString nmat) { this->matricula_usuario = nmat; }
    void setAvaliacao(int naval) { this->avaliacao = naval; }
    void setTipo(int ntipo) { this->tipo = ntipo; }

    void adicionar();
    void deletar();
};

#endif // REFEICAO_H

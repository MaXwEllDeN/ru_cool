#-------------------------------------------------
#
# Project created by QtCreator 2017-03-26T00:36:06
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RU_Cool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    tserial.cpp \
    deskwindow.cpp \
    usuario.cpp \
    refeicao.cpp \
    userviewdialog.cpp \
    aboutwindow.cpp

HEADERS  += mainwindow.h \
    database.h \
    tserial.h \
    deskwindow.h \
    usuario.h \
    refeicao.h \
    userviewdialog.h \
    aboutwindow.h

FORMS    += mainwindow.ui \
    deskwindow.ui \
    userviewdialog.ui \
    aboutwindow.ui

QMAKE_CXXFLAGS +=  -std=c++11 -pthread

RESOURCES += \
    imagens.qrc

RC_FILE = myapp.rc

#ifndef DESKWINDOW_H
#define DESKWINDOW_H

#include "usuario.h"

#include <QMainWindow>

#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
class DeskWindow;
}

class DeskWindow : public QMainWindow
{
    Q_OBJECT

private slots:
    void on_userClicked(QString);
    void onDataRead(QString);
    void on_pictureDownloaded(QNetworkReply*);

    void on_actionSobre_triggered();
    void setAvaliacao(int percent);

public:
    explicit DeskWindow(QWidget *parent = 0);
    void updateLastUsers();
    int getAvaliacao();
    QString getComport() { return comport; }
    ~DeskWindow();

private:
    QString comport;
    Ui::DeskWindow *ui;
    int refeicao_id;

    QNetworkAccessManager* networkAccess;
    QNetworkReply* networkReply;
};

#endif // DESKWINDOW_H

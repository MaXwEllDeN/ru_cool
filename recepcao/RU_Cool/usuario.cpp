#include "usuario.h"
#include "database.h"
#include <QDebug>
#include <QDateTime>

Usuario::Usuario()
{
	for (int i = 0; i < 6; i++)
		dados.push_back("");
}

Usuario::Usuario(QString matricula)
{
    this->dados = Database::getInstance()->returnData("SELECT matricula, nome, email, curso, senha, cartao_id FROM usuarios WHERE matricula = :matricula;", 5, std::vector<QString>{matricula});
}

void Usuario::deletar()
{
    Database::getInstance()->execute("DELETE FROM usuarios WHERE matricula = :matricula;", std::vector<QString>{dados[0]});
}

Refeicao Usuario::getRecentRefeicao()
{
    QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QStringList aux = data.split(" ")[1].split(":");

    unsigned int hours = aux[0].toInt() - 2;

    if (hours > 24)
        hours = 0;

    data = data.split(" ")[0] + " " + QString::number(hours) + ":" + aux[1] + ":" + aux[2];
    QString sql = "SELECT id, tipo, avaliacao, matricula, data FROM refeicoes WHERE matricula = '" + dados[0]
                  + "' AND data > '"+ data +"' ORDER BY data DESC LIMIT 1;";

    std::vector<QString> dados_ref = Database::getInstance()->returnData(sql, 5, std::vector<QString>{});

    if (dados_ref.size() == 0) {
        return Refeicao(-1);
    }


    return Refeicao(dados_ref);
}

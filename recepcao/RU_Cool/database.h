#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <vector>
#include <QList>
#include <vector>

class Database
{
private:
    static Database* instance;

    QSqlDatabase mDatabase;
    QSqlQuery mQuery;

public:
    static Database* getInstance();
    std::vector<QString> returnData(QString query, int results, std::vector<QString> vec);
    void execute(QString query, std::vector<QString> vec);
    bool queryCheck(QString query, std::vector<QString> vec);

    void close();
    QSqlDatabase* getQtDatabase();

private:
    Database(){} ;
    Database(const Database&);
    Database& operator =(const Database&);
    void connect();

};

#endif // DATABASE_H


#ifndef USUARIO_H
#define USUARIO_H

#include "refeicao.h"

#include <QString>
#include <vector>

class Usuario
{
private:
    std::vector<QString> dados;
public:
    Usuario();
    Usuario(QString matricula);
    void deletar();

    QString getMatricula() { return dados[0];}
    QString getNome() { return dados[1];}
    QString getCurso() { return dados[3];}

    Refeicao getRecentRefeicao();
};

#endif // USUARIO_H

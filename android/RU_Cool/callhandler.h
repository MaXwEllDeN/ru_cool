#ifndef CALLHANDLER_H
#define CALLHANDLER_H

#include <QObject>
#include <QVariantList>
#include "database.h"

class CallHandler : public QObject
{
    Q_OBJECT
public:
    explicit CallHandler(QObject *parent = 0);

    Q_INVOKABLE bool login(QString matricula, QString senha);
    Q_INVOKABLE QVariantList getNoticias(int limit);
    Q_INVOKABLE QVariantList getUserInfo(QString matricula);
    Q_INVOKABLE void setAvaliacao(QString matricula, QString valor);
    Q_INVOKABLE bool canEvaluate(QString matricula);

signals:

public slots:
};

#endif // CALLHANDLER_H

import QtQuick 2.4
import Material 0.2
import QtQuick.Layouts 1.1
import Material.ListItems 0.1 as ListItem
import Material.Extras 0.1
import QtGraphicalEffects 1.0

Item {
    id: itemRoot
    implicitHeight: column.height
    anchors.centerIn: parent
    height: 800
    property var infoArray: chandler.getUserInfo(mainapp.matricula)

    View {
        id: userInfo

        elevation: 1
        radius: 2

        height: 700
        anchors {
            fill: parent
            margins: 32
        }

        ColumnLayout {
            id: column

            height: 200
            width: 400

            anchors {
                right: parent.right
                left: parent.left
                margins: 30
            }

            ListItem.Standard {
                Label {
                    id: titleLabel

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: 16
                    }

                    style: "title"
                    text: infoArray[0]
                }
            }

            Item {
                id: sep1
                Layout.fillWidth: true
            }


            Item {
                id : imgItem
                anchors {
                    top: sep1.bottom
                    topMargin: 10
                    left: parent.left
                    leftMargin: 10
                }

                width: imgProfile.width
                height: imgProfile.height

                Image {
                  id: imgProfile
                  asynchronous: true
                  height: 350
                  width: 270

                  source: "http://192.168.173.1/imagens/" + mainapp.matricula
                }
            }

            Text {
                id: lblMatricula
                font.pointSize: 14
                anchors {
                    top: imgItem.top
                    left: imgItem.right
                    leftMargin: 30
                }

                text: "Matrícula: " + mainapp.matricula
            }

            Text {
                id: lblEmail
                font.pointSize: 14
                anchors {
                    top: lblMatricula.bottom
                    left: imgItem.right
                    topMargin: 20
                    leftMargin: 30
                }

                text: "Email: " + infoArray[1]
            }

            Text {
                id: lblCurso
                font.pointSize: 14
                anchors {
                    top: lblEmail.bottom
                    left: imgItem.right
                    topMargin: 20
                    leftMargin: 30
                }

                text: "Curso: " + infoArray[2]
            }

            Text {
                id: lblAlmoco
                font.pointSize: 14
                anchors {
                    top: lblCurso.bottom
                    left: imgItem.right
                    topMargin: 20
                    leftMargin: 30
                }

                text: {

                    if (infoArray[3] == 1)
                        return "Você almoçou " + infoArray[3] + " vez no RU"
                    else if (infoArray[3] > 1)
                        return "Você almoçou " + infoArray[3] + " vezes no RU"
                    else
                        return "Você nunca almoçou no RU"
                }
            }

            Text {
                id: lblJantares
                font.pointSize: 14
                anchors {
                    top: lblAlmoco.bottom
                    left: imgItem.right
                    topMargin: 20
                    leftMargin: 30
                }

                text: {

                    if (infoArray[4] == 1)
                        return "Você jantou " + infoArray[4] + " vez no RU"
                    else if (infoArray[4] > 1)
                        return "Você jantou " + infoArray[4] + " vezes no RU"
                    else
                        return "Você nunca jantou no RU"
                }
            }

            Text {
                id: lblAvaliacao
                font.pointSize: 14
                anchors {
                    top: lblJantares.bottom
                    left: imgItem.right
                    topMargin: 20
                    leftMargin: 30
                }

                text: {
                    if (infoArray[5] == 0)
                        return "Avaliação média: nenhuma"
                    else if (infoArray[5] < 2)
                        return "Avaliação média: " + infoArray[5] + " estrela"
                    else
                        return "Avaliação média: " + infoArray[5] + " estrelas"
                }
            }

            Button {
                anchors {
                    top: lblAvaliacao.bottom
                    topMargin: 20
                }

                visible: chandler.canEvaluate(mainapp.matricula)
                text: "Avaliar última refeição"
                textColor: Theme.primaryColor

                onClicked: {
                    mainapp.selectedComponent = "Avaliação";
                }
           }

        }

    }
}

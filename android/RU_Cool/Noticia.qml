import QtQuick 2.4
import Material 0.2

import QtQuick.Layouts 1.1
import Material.ListItems 0.1 as ListItem
import QtQuick.Controls 1.3 as Controls
import Material.Extras 0.1


View {
    id: idnoticia

    elevation: 1
    radius: 2

    property var titulo: "TITLE MUST BE INITIALIZED"
    property var conteudo_texto: "CONTENT MUST BE INITIALIZED"
    property var data_texto: "DATE MUST BE INITIALIZED"

    height: 400

    signal changeHeight(real n_height)

    onChangeHeight: {
        Layout.preferredHeight = n_height
    }

    ColumnLayout {
        id: column

        height: 200
        width: 350

        anchors {
            fill: parent
            topMargin: 16
            bottomMargin: 16
        }

        ListItem.Standard {
            Label {
                id: titleLabel

                anchors {
                    left: parent.left
                    right: parent.right
                    margins: 16
                }

                style: "title"
                text: titulo
            }
        }

        Item {
            id: sep1
            Layout.fillWidth: true
        }

        Text {
            id: content
            anchors {
                top: sep1.bottom
                left: parent.left
                leftMargin: 50
                rightMargin: 50
            }

            text: conteudo_texto
            horizontalAlignment: Text.AlignJustify
            Layout.preferredWidth: parent.width - 100
            wrapMode: TextEdit.WordWrap

            onTextChanged: idnoticia.changeHeight(idnoticia.height + parseInt(conteudo_texto.length / 78) * 150)
        }



        Item {
            height: 300
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Label {
            anchors {
                right: parent.right
                bottom: parent.bottom
            }

            text: data_texto
        }
    }

}

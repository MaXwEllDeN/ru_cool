TEMPLATE = app

QT += qml quick widgets svg sql
CONFIG += c++11

SOURCES += \
    callhandler.cpp \
    main.cpp \
    database.cpp

RESOURCES += qml.qrc \
    icons/icons.qrc

HEADERS += \
    callhandler.h \
    database.h

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android




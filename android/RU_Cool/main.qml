import QtQuick 2.4
import Material 0.2
import QtQuick.Window 2.2
import Material.ListItems 0.1 as ListItem


ApplicationWindow {
    id: mainapp

    title: "RU Cool"
    visible: true
    height: 600
    width: 300

    property var matricula: ""

    function dp(pixel) {
        return  pixel * Screen.pixelDensity
    }

    theme {
        primaryColor: "orange"
        accentColor: "red"
        tabHighlightColor: "darkgreen"
    }

    property string selectedComponent: "Login"
    property string page_title: "Login"

    initialPage: TabbedPage {
        id: page

        title: page_title

        backAction: navDrawer.action

        NavigationDrawer {
            id: navDrawer
            enabled: matricula != ""

            //onEnabledChanged: smallLoader.active = enabled

            Flickable {
                anchors.fill: parent

                contentHeight: Math.max(content.implicitHeight, height)

                Column {
                    id: content
                    anchors.fill: parent

                    Column {
                        width: parent.width

                        ListItem.Subheader {
                            text: "RU Cool"
                        }

                        ListItem.Standard {
                            text: "Notícias"
                            selected: mainapp.selectedComponent == "Noticias"
                            iconName: "communication/chat"
                            onClicked: {
                                mainapp.selectedComponent = "Noticias";
                                navDrawer.close()
                            }
                        }

                        ListItem.Subheader {
                            text: "Seu cadastro"
                        }

                        ListItem.Standard {
                            text: "Informações"
                            selected: mainapp.selectedComponent == "Informações"
                            iconName: "action/account_circle"
                            onClicked: {
                                mainapp.selectedComponent = "Informações";
                                navDrawer.close()
                            }
                        }

                        ListItem.Standard {
                            text: "Logout"
                            iconName: "action/exit_to_app"
                            onClicked: {
                                mainapp.selectedComponent = "Login";
                                mainapp.matricula = ""
                                navDrawer.close()
                            }
                        }
                    }

                }
            }
        }


        Tab {
            title: ""
            property string selectedComponent: ""
            property var section: ""
            sourceComponent: tabDelegate
        }


        Loader {
            id: smallLoader
            anchors.fill: parent
            sourceComponent: tabDelegate

            property var section: []
            visible: active
            active: false
        }
    }


    Component {
        id: tabDelegate

        Item {
            Sidebar {
                id: sidebar

                expanded: false

                Column {
                    width: parent.width
                }
            }

            Flickable {
                id: flickable

                signal changeHeight()

                anchors {
                    left: sidebar.right
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }

                clip: true
                contentHeight: Math.max(mainLoader.implicitHeight, height) + dp(200)

                Loader {
                    id: mainLoader

                    anchors {
                        top: parent.top
                        topMargin: 100
                        left: parent.left
                        right: parent.right
                    }

                    asynchronous: true
                    visible: status == Loader.Ready
                    source: {                        
                        mainapp.page_title = mainapp.selectedComponent
                        Qt.resolvedUrl(mainapp.selectedComponent + ".qml")
                    }                 

                }

                Connections {
                    target: mainLoader

                    onLoaded: {
                        flickable.contentHeight = mainLoader.height + 250
                    }
                }

                ProgressCircle {
                    anchors.centerIn: parent
                    visible: mainLoader.status == Loader.Loading
                }
            }


            Scrollbar {
                flickableItem: flickable
            }

        }
    }
}

import QtQuick 2.4
import Material 0.2

import QtQuick.Layouts 1.1
import Material.ListItems 0.1 as ListItem
import QtQuick.Controls 1.3 as Controls
import Material.Extras 0.1

Item{
    id: loginpage
    implicitHeight: column.height
    anchors.centerIn: parent
    width: 1000

    View {
        id: viewzin
        anchors.centerIn: parent

        height: column.implicitHeight
        width: 800

        elevation: 1
        radius: 2


        ColumnLayout {
            id: column
            anchors {
                fill: parent
                topMargin: 16
                bottomMargin: 16
            }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                Layout.preferredHeight: 270
                Layout.preferredWidth: 700
                source: "/res/res/ru_cool.png"
            }

            Label {
                id: titleLabel

                anchors {
                    left: parent.left
                    right: parent.right
                    margins: 16
                }

                style: "title"
                text: "Informe seus dados"
            }

            Item {
                Layout.fillWidth: true
            }

            ListItem.Standard {
                action: Icon {
                    anchors.centerIn: parent
                    name: "social/person"
                }

                content: TextField {
                    id: tfmatricula
                    anchors.centerIn: parent
                    width: parent.width
                    placeholderText: "Matrícula"
                    floatingLabel: true
                }
            }

            ListItem.Standard {
                action: Icon {
                    anchors.centerIn: parent
                    name: "communication/vpn_key"
                }

                content: TextField {
                    id: tfsenha
                    anchors.centerIn: parent
                    width: parent.width
                    echoMode: TextInput.Password

                    placeholderText: "Senha"
                    floatingLabel: true
                }
            }

            Item {
                Layout.fillWidth: true
            }

            RowLayout {
                Layout.alignment: Qt.AlignCenter
                spacing: 8

                anchors {
                    right: parent.right
                    margins: 16
                }

                Button {
                    text: "Entrar"
                    textColor: Theme.primaryColor

                    onClicked: {                        
                        if (!chandler.login(tfmatricula.text, tfsenha.text)) {
                            snackbar.open("A matrícula e senha não coincidem!");
                        }
                        else {
                            mainapp.matricula = tfmatricula.text
                            mainapp.selectedComponent = "Noticias";
                        }
                    }

                    anchors.bottomMargin: 100
                }
            }

        }

        Snackbar {
            id: snackbar
        }
    }

}

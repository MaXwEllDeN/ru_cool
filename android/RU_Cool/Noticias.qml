import QtQuick 2.4
import Material 0.2
import QtQuick.Layouts 1.1
import Material.ListItems 0.1 as ListItem
import Material.Extras 0.1

Item {
    id: itemRoot
    implicitHeight: column.height
    anchors.centerIn: parent

    View {
        id: content
        anchors {
            fill: parent
            margins: 32
        }

        ColumnLayout {
            id: column
            anchors {
                right: parent.right
                left: parent.left
                margins: 30
            }

            spacing: 100

            Repeater {
                model: chandler.getNoticias(5);

                delegate:
                    Noticia {
                        anchors.right: parent.right
                        anchors.left: parent.left

                        titulo: modelData[0]
                        conteudo_texto: modelData[1]
                        data_texto: modelData[2]
                }
            }

            Item {
                Layout.fillWidth: true
                height: 20
            }
        }
    }
}

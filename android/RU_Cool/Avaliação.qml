import QtQuick 2.4
import Material 0.2
import QtQuick.Layouts 1.1
import Material.ListItems 0.1 as ListItem
import Material.Extras 0.1

Item {
    id: itemRoot
    implicitHeight: column.height
    anchors.centerIn: parent
    height: 700

    View {
        id: userInfo

        elevation: 1
        radius: 2

        height: 700
        anchors {
            fill: parent
            margins: 32
        }

        ColumnLayout {
            id: column

            height: 200
            width: 400

            anchors {
                right: parent.right
                left: parent.left
                margins: 30
            }

            ListItem.Standard {

                Label {
                    id: titleLabel

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: 16
                    }

                    style: "title"
                    text: "Avalie sua última refeição"
                }

            }

            Item {
                id: sep1
                Layout.fillWidth: true
            }

            Slider {
                Layout.alignment: Qt.AlignCenter
                id: sliderValue
                value: 80
                focus: true
                tickmarksEnabled: true
                numericValueLabel: true
                stepSize: 1
                minimumValue: 1
                maximumValue: 5
                color: "#f4c842"
                activeFocusOnPress: true
            }


            Button {
                anchors.right: column.right
                text: "Enviar"
                textColor: Theme.primaryColor

                onClicked: {
                    mainapp.selectedComponent = "Informações"
                    chandler.setAvaliacao(mainapp.matricula, sliderValue.value)

                }

            }
        }
    }
}

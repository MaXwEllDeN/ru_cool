#include "callhandler.h"
#include "database.h"

#include <vector>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>

CallHandler::CallHandler(QObject *parent) : QObject(parent) {}

bool CallHandler::login(QString matricula, QString senha)
{
    std::vector<QString> dados = Database::getInstance()->returnData("SELECT nome FROM usuarios WHERE matricula = :mat AND senha = :sen;", 1, std::vector<QString>{matricula, senha});


    return dados.size() > 0;
}

QVariantList CallHandler::getNoticias(int limit)
{
    QVariantList noticias;

    QSqlQuery query = Database::getInstance()->getQtDatabase()->exec();
    query.prepare("SELECT titulo, texto, data FROM noticias ORDER BY data DESC LIMIT :im;");
    query.bindValue(0, limit);
    query.exec();

    while (query.next()) {
        QStringList noticia;
        noticia.push_back(query.value(0).toString());
        noticia.push_back(query.value(1).toString());

        QString aux = query.value(2).toString();
        QStringList dia = (aux.split("T")[0]).split("-");
        QStringList horario = (aux.split("T")[1]).split(":");

        noticia.push_back("Postado em " + dia[2] + "/" + dia[1] + "/" + dia[0] + " às " + horario[0] + " hora(s) e " + horario[1] + " minuto(s).");

        noticias.push_back(noticia);
    }

    return noticias;
}

QVariantList CallHandler::getUserInfo(QString matricula)
{
    QVariantList info;

    QSqlQuery query = Database::getInstance()->getQtDatabase()->exec();
    query.prepare("SELECT nome, email, curso FROM usuarios WHERE matricula = :mat;");
    query.bindValue(0, matricula);
    query.exec();

    if (query.next()) {
        info.push_back(query.value(0).toString());
        info.push_back(query.value(1).toString());
        info.push_back(query.value(2).toString());
    }

    int almoco = 0;
    int jantar = 0;

    double avaliacao = 0;
    double qnt_avaliacao = 0;

    query = Database::getInstance()->getQtDatabase()->exec();
    query.prepare("SELECT tipo, avaliacao FROM refeicoes WHERE matricula = :mat;");
    query.bindValue(0, matricula);
    query.exec();

    while (query.next()) {
        if (query.value(0) == 1)
            almoco++;
        else
            jantar++;

        if (query.value(1).toInt() > -1) {
            avaliacao += query.value(1).toDouble();
            qnt_avaliacao++;
        }
    }

    info.push_back(QString::number(almoco));
    info.push_back(QString::number(jantar));

    if (qnt_avaliacao != 0)
        info.push_back(QString::number(avaliacao / qnt_avaliacao, 'f', 2));
    else
        info.push_back(QString::number(0, 'f', 2));

    return info;
}

void CallHandler::setAvaliacao(QString matricula, QString valor)
{
    QSqlQuery query = Database::getInstance()->getQtDatabase()->exec();
    query.prepare("SELECT id FROM refeicoes WHERE matricula = :mat ORDER BY data DESC LIMIT 1;");
    query.bindValue(0, matricula);
    query.exec();

    QString id;

    if (query.next())
        id = query.value(0).toString();

    query.prepare("UPDATE refeicoes SET avaliacao = :aval WHERE id = :do;");
    query.bindValue(0, valor);
    query.bindValue(1, id);

    query.exec();

}

bool CallHandler::canEvaluate(QString matricula)
{
    QSqlQuery query = Database::getInstance()->getQtDatabase()->exec();

    QString data = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QStringList aux = data.split(" ")[1].split(":");

    unsigned int hours = aux[0].toInt() - 2;

    if (hours > 24)
        hours = 0;

    data = data.split(" ")[0] + " " + QString::number(hours) + ":" + aux[1] + ":" + aux[2];

    query.prepare("SELECT id FROM refeicoes WHERE matricula = " + matricula+ " AND data > '"+ data +"' AND avaliacao = '-1' ORDER BY data DESC LIMIT 1;");
    query.exec();

    if (query.next())
        return true;

    return false;
}
